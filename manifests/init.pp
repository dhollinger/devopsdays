# DevOpsDays KC 2018 Module that deploys a simple LEMP server
# with NGINX, PHP, and MySQL on a Linux Server.
#
# @summary Sets up NGINX, PHP, and MySQL for DevOpsDays workshop
#
# @author David Hollinger <david.hollinger@moduletux.com>
#
# @see https://forge.puppet.com/puppetlabs/stdlib Puppet Stdlib Module
# @see https://forge.puppet.com/puppet/php PHP Module
# @see https://forge.puppet.com/puppetlabs/mysql MySQL Module
# @see https://forge.puppet.com/puppet/nginx Nginx Module
# @see https://nginx.org/en/docs/ Nginx Docs
# @see https://secure.php.net/docs.php PHP Docs
# @see https://secure.php.net/manual/en/install.fpm.php PHP FPM Docs
# @see https://dev.mysql.com/doc/ MySQL Docs
# @see https://mariadb.com/kb/en/library/documentation/ MariaDB Docs
# @see https://forge.puppet.com/puppetlabs/firewall IPTables module
# @see https://netfilter.org/documentation/ IPTables docs
#
# @example
#   class { 'devopsdays':
#     fix_cgi          => true,
#     php_extensions   => {
#       mysqli         => {},
#       mysqlnd        => {},
#     },
#     fpm_listen_owner => 'www-data',
#     fpm_listen_group => 'www-data',
#     fpm_user         => 'www-data',
#     fpm_group        => 'www-data',
#     fpm_skt_dir      => '/var/run/php/php-fpm.sock',
#   }
#
# @param fix_cgi
#   Whether or not to enable the cgi.fix_pathinfo setting in PHP FPM
# @param php_extensions
#   Hash of PHP extensions to install and settings.
# @param fpm_listen_owner
#   Owner of the socket listener
# @param fpm_listen_group
#   Group that owns the socket listener
# @param fpm_user
#   User that owns the PHP FPM process
# @param fpm_group
#   Group that owns the PHP FPM process
# @param fpm_skt_dir
#   Path to the PHP FPM Socket file.
#
class devopsdays (
  Boolean $fix_cgi,
  Hash $php_extensions,
  String $fpm_listen_owner,
  String $fpm_listen_group,
  String $fpm_user,
  String $fpm_group,
  Stdlib::Absolutepath $fpm_skt_dir,
) {
  contain devopsdays::firewall
  contain devopsdays::nginx
  contain devopsdays::mysql
  contain devopsdays::php
}
