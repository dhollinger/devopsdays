# Class that sets up Nginx virtual host for RHEL-based systems
#
# @summary Configure Nginx for RHEL-based systems
#
# @api private
#
# @author David Hollinger <david.hollinger@moduletux.com>
#
# @see devopsdays::nginx
# @see https://forge.puppet.com/puppet/nginx Nginx Module
# @see https://nginx.org/en/docs/ Nginx Docs
#
class devopsdays::nginx::el {
  assert_private()

  file { '/usr/share/nginx/html/info.php':
    ensure  => file,
    content => template('devopsdays/info.php'),
    require => Package['nginx'],
    notify  => Service['nginx'],
  }

  nginx::resource::server { 'example.com':
    ensure               => 'present',
    listen_port          => 80,
    www_root             => '/usr/share/nginx/html',
    use_default_location => false,
    index_files          => [
      'index.php',
      'index.html',
      'index.htm',
    ],
    server_name          => ['example.com'],
  }

  nginx::resource::location { 'php_files_location':
    ensure        => 'present',
    server        => 'example.com',
    location      => '~ \.php$',
    fastcgi       => 'unix:/var/run/php-fpm/php-fpm.sock',
    fastcgi_index => 'index.php',
    fastcgi_param => {
      'SCRIPT_FILENAME' => '$document_root$fastcgi_script_name',
    },
    priority      => 501,
  }

  nginx::resource::location { '50x_errors':
    ensure   => 'present',
    server   => 'example.com',
    www_root => '/usr/share/nginx/html',
    location => '= /50x.html',
    priority => 500,
  }
}
